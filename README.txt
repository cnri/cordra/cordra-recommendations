Cordra Recommendations Module

The Cordra Recommendations Module is a java web app that communicates
with Cordra and SOLR to produce tf-idf based item recommendations.

In order to use this functionality you need to create user objects in
your Cordra instance. Items that can be rated can be any other Cordra
object.

To install this module copy the cordra-recommendations.war file in to
Cordra's data/webapps/ directory.

A file called "recommendations-config.json" should also be created in
Cordra's data/webapps-storage/recommendations/ folder.

Example recommendations-config.json:

{
  "cordraBaseUri" : "http://localhost:8081/cordra/",
  "solrBaseUri" : "http://localhost:8983/solr/cordra",
  "cordraAdminPassword" : "password",
  "enableSimilarProfileMode" : true
}


There are two modes of operation, "Profile" based and
"Similar Profiles" based.


Profile Based Recommendations:

To generate profile based recommendations first the vectors for the
items the user has rated are retrieved, optionally these vectors are
boosted by weighting them by the specified rating. The mean of these
vectors is then calculated resulting in a taste vector for the
user. That taste vector is then converted into a query, where each
term in the query is boosted by the magnitude of that term in the
taste vector. The results of the query are the recommendations.


Similar Profiles Based Recommendations:

When a user rates an item the json content of that item is copied into
an array in the users profile, stored on the User object. This results
in a user profile being the union of the items they have rated. These
user profiles are indexed in the usual way. To generate
recommendations for a user first the vector for their profile is
retrieved and used to search for other profiles that are most similar
to their own. The vectors for the items that group of similar users
have rated are then retrieved, optionally weighted by the ratings and
the mean vector is calculated. This resulting taste vector represents
the taste of the user in question combined with the taste of user who
share similar taste.  This collaborative taste vector is used to query
for recommendations as before.

The similar profiles based approach is an ensemble recommendations
method that combines collaborative filtering with content based
filtering.

Note that similar profile based recommendations only consider the Json
content of items (not the payloads) for the collaborative component of
the algorithm.


If you initially have have the property "enableSimilarProfileMode" set
to false and then later change it to true that will not modify the
existing user profile objects. Only items rated going forwards will
use this feature.


SOLR Configuration:

This module requires SOLR to be configured as the indexer for
Cordra. Two SOLR configuration files are included with this
distribution, solrconfig.xml and managed-schema. These should be
copied into the configuration directory solr/cordra/conf for your
cordra index.


Example API calls:

All calls require credentials in the form of the 'Basic' HTTP
Authentication Scheme.


Rating an item:

Ratings must be integers in the range 1 to 5 inclusive. Unlike star
ratings, all ratings here are positive. If you do not like an item, do
not rate it.

curl 'http://localhost:8081/cordra-recommendations/rateItem/?itemId=test/197&rating=5' -H 'Authorization: Basic YmVuOnBhc3N3b3Jk' -X POST


Liking an item (equivalent to giving an item a rating of 1):

curl 'http://localhost:8081/cordra-recommendations/rateItem/?itemId=test/197' -H 'Authorization: Basic YmVuOnBhc3N3b3Jk' -X POST


Deleting a rating:

curl 'http://localhost:8081/cordra-recommendations/rateItem/?itemId=test/197' -H 'Authorization: Basic YmVuOnBhc3N3b3Jk' -X DELETE


Getting recommendations for a user:

curl 'http://localhost:8081/cordra-recommendations/recommendations/' -H 'Authorization: Basic YmVuOnBhc3N3b3Jk'


Getting recommendations for a user boosted by their item ratings:

Boosting by ratings means the individual vectors for rated items are
weighted by the users rating prior to being combined into the taste
vector for the user.

curl 'http://localhost:8081/cordra-recommendations/recommendations/?boost=true' -H 'Authorization: Basic YmVuOnBhc3N3b3Jk'


Getting recommendations for a user considering profiles similar to this user:

curl 'http://localhost:8081/cordra-recommendations/recommendations/?mode=similarProfiles' -H 'Authorization: Basic YmVuOnBhc3N3b3Jk'


Finding items that are similar to a single specified item:

curl 'http://localhost:8081/cordra-recommendations/moreLikeThis/?id=test/197' -H 'Authorization: Basic YmVuOnBhc3N3b3Jk'


Finding items that are similar to a set of specified items:

curl 'http://localhost:8081/cordra-recommendations/moreLikeThis/?id=test/197&id=test/198&id=test/199' -H 'Authorization: Basic YmVuOnBhc3N3b3Jk'
