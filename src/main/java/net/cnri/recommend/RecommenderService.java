package net.cnri.recommend;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.cnri.cordra.api.*;
import org.apache.solr.client.solrj.SolrClient;

import java.util.ArrayList;
import java.util.List;

public class RecommenderService {

    private final CordraClient cordra;
    private final SolrClient solr;
    private final String cordraBaseUri;
    private final boolean enableSimilarProfileMode;

    public static enum Mode {
        SIMPLE_CONTENT_BASED,
        SIMILAR_PROFILE_BASED
    }

    public RecommenderService(String cordraBaseUri, CordraClient cordra, SolrClient solr, boolean enableSimilarProfileMode) {
        this.cordraBaseUri = cordraBaseUri;
        this.cordra = cordra;
        this.solr = solr;
        this.enableSimilarProfileMode = enableSimilarProfileMode;
    }

    public void rateItem(String userId, String itemId, Integer rating, String username, String password) throws Exception {
        try (CordraClient cordraClient = new TokenUsingHttpCordraClient(cordraBaseUri, username, password)) {
            if (rating != null && (rating < 1 || rating > 5)) {
                throw new BadRequestCordraException("Rating must be between 1 and 5 inclusive");
            }
            CordraObject userCo = cordraClient.get(userId, username, password);
            if (userCo == null) {
                throw new BadRequestCordraException("There is no user " + userId);
            }
            JsonObject user = userCo.content.getAsJsonObject();
            CordraObject itemCo = cordraClient.get(itemId, username, password);
            if (itemCo == null) {
                throw new BadRequestCordraException("There is no item " + itemId);
            }
            if ("User".equals(itemCo.type)) {
                throw new BadRequestCordraException("The system does not support Users rating User objects. The item your rated is of type User");
            }
            JsonObject item = itemCo.content.getAsJsonObject();
            addItemRatingToUserJson(user, item, itemId, rating);
            userCo.setContent(user);
            cordraClient.update(userCo, username, password);
        }
    }

    public void deleteRating(String userId, String itemId, String username, String password) throws Exception {
        try (CordraClient cordraClient = new TokenUsingHttpCordraClient(cordraBaseUri, username, password)) {
            CordraObject userCo = cordraClient.get(userId, username, password);
            if (userCo == null) {
                throw new BadRequestCordraException("There is no user " + userId);
            }
            JsonObject user = userCo.content.getAsJsonObject();
            if (!user.has("itemRatings")) {
                throw new BadRequestCordraException("User " + userId + " has not rated item " + itemId);
            }
            JsonArray itemRatings = user.getAsJsonArray("itemRatings");
            int position = positionOfRatedItem(itemRatings, itemId);
            if (position == -1) {
                throw new BadRequestCordraException("User " + userId + " has not rated item " + itemId);
            }
            itemRatings.remove(position);
            userCo.setContent(user);
            cordraClient.update(userCo, username, password);
        }

    }

    private void addItemRatingToUserJson(JsonObject user, JsonObject item, String itemId, Integer rating) {
        JsonObject itemRating = new JsonObject();
        itemRating.addProperty("id", itemId);
        if (rating != null) {
            itemRating.addProperty("rating", rating);
        }
        if (item != null && enableSimilarProfileMode) {
            itemRating.add("item", item);
        }
        JsonArray itemRatings;
        if (user.has("itemRatings")) {
            itemRatings = user.getAsJsonArray("itemRatings");
        } else {
            itemRatings = new JsonArray();
            user.add("itemRatings", itemRatings);
        }
        int existingPosition = positionOfRatedItem(itemRatings, itemId);
        if (existingPosition == -1) {
            itemRatings.add(itemRating);
        } else {
            itemRatings.set(existingPosition, itemRating);
        }
    }

    private int positionOfRatedItem(JsonArray itemRatings, String itemId) {
        for (int i = 0; i < itemRatings.size(); i++) {
            if (itemId.equals(itemRatings.get(i).getAsJsonObject().get("id").getAsString())) {
                return i;
            }
        }
        return -1;
    }

    private boolean hasRatedItem(JsonArray itemRatings, String itemId) {
        for (int i = 0; i < itemRatings.size(); i++) {
            if (itemId.equals(itemRatings.get(i).getAsJsonObject().get("id").getAsString())) {
                return true;
            }
        }
        return false;
    }

    private SearchResults<CordraObject> searchForRecommendations(
        String userId, CordraClient cordraClient, List<String> limitedFields, boolean boost, Mode mode) throws Exception {
        if (mode == Mode.SIMPLE_CONTENT_BASED) {
            return Recommender.getSimpleContentBasedPersonalRecommendations(userId, cordraClient, solr, limitedFields, boost);
        } else {
            return Recommender.getSimilarProfileBasedRecommendations(userId, cordraClient, solr, limitedFields, boost);
        }
    }

    public List<String> getRecommendations(
        String userId, int num, boolean boost, Mode mode, String username, String password) throws Exception {
        List<String> limitedFields = null;
        List<String> ids = new ArrayList<>();
        try (CordraClient cordraClient = new TokenUsingHttpCordraClient(cordraBaseUri, username, password)) {
            CordraObject userCo = cordraClient.get(userId);
            JsonObject profile = userCo.content.getAsJsonObject();
            if (profile.has("itemRatings")) {
                JsonArray itemRatings = profile.get("itemRatings").getAsJsonArray();
                if (itemRatings.size() == 0) {
                    return ids;
                }
            } else {
                return ids;
            }

            try (
                SearchResults<CordraObject> results = searchForRecommendations(userId, cordraClient, limitedFields, boost, mode)
                ) {
                int count = 0;
                for (CordraObject co : results) {
                    ids.add(co.id);
                    count++;
                    if (count == num) {
                        break;
                    }
                }
            }

        }
        return ids; 
    }

    public String getUserIdForUsername(String username) throws CordraException {
        String userId = null;
        String q = "username:\"" + username + "\"";
        try (SearchResults<CordraObject> results = cordra.search(q)) {
            for (CordraObject co : results) {
                String foundUsername = co.content.getAsJsonObject().get("username").getAsString();
                if (username.equalsIgnoreCase(foundUsername)) {
                    userId = co.id;
                }
            }
        } 
        return userId;
    }

    private void throwIfNotAllExists(List<String> ids, CordraClient cordraClient) throws Exception {
        for (String id : ids) {
            throwIfNotExists(id, cordraClient);
        }
    }

    private void throwIfNotExists(String id, CordraClient cordraClient) throws Exception {
        if (cordraClient.get(id) == null) {
            throw new BadRequestCordraException("id: " + id + " does not exist");
        }
    }

    public List<String> getMoreLikeThis(String userId, List<String> ids, String username, String password, int num) throws Exception {
        try (CordraClient cordraClient = new TokenUsingHttpCordraClient(cordraBaseUri, username, password)) {
            throwIfNotAllExists(ids, cordraClient);
            try (
                SearchResults<CordraObject> results = Recommender.moreLikeThis(ids, cordraClient, solr);
                ) {
                int count = 0;
                List<String> result = new ArrayList<>();
                for (CordraObject co : results) {
                    result.add(co.id);
                    count++;
                    if (count == num) {
                        break;
                    }
                }
                return result;
            }
        }
    }
}
