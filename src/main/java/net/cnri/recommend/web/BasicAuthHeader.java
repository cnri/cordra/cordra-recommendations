package net.cnri.recommend.web;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * A container for a username and a password, obtained by parsing an HTTP Basic auth Authorization: header.
 */
public class BasicAuthHeader {
    private String username = null;
    private String password = null;

    /**
     * Constructor for a {@code BasicAuthHeader}.  Decodes the username and password from an HTTP Basic auth Authorization: header.
     *
     * @param authHeader The value of an HTTP Basic auth Authorization: header.
     */
    public BasicAuthHeader(String authHeader) {
        if (authHeader != null) {
            try {
                String encodedUsernameAndPassWord = getEncodedUserNameAndPassword(authHeader);
                String decodedAuthHeader = new String(Base64.getDecoder().decode(encodedUsernameAndPassWord.getBytes()));
                username = decodedAuthHeader.substring(0, decodedAuthHeader.indexOf(":"));
                password = decodedAuthHeader.substring(decodedAuthHeader.indexOf(":") + 1);
            } catch (Exception e) {
                // ignore and let be anonymous
            }
        }
    }

    public BasicAuthHeader(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getAuthHeader() {
        byte[] usernamePasswordBytes = (username + ":" + password).getBytes(StandardCharsets.UTF_8);
        String usernamePasswordBase64 = Base64.getEncoder().encodeToString(usernamePasswordBytes);
        return "Basic " + usernamePasswordBase64;
    }

    private String getEncodedUserNameAndPassword(String authHeader) {
        return authHeader.substring(authHeader.indexOf(" ") + 1).trim();
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}
