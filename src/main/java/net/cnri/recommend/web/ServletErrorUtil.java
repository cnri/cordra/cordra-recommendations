/*************************************************************************\
    Copyright (c) 2019 Corporation for National Research Initiatives;
                        All rights reserved.
\*************************************************************************/

package net.cnri.recommend.web;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class ServletErrorUtil {
    private static Gson gson = new Gson();

    public static void internalServerError(HttpServletResponse resp) throws IOException {
        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        CordraErrorResponse errorResponse = new CordraErrorResponse("Something went wrong. Contact your sysadmin.");
        gson.toJson(errorResponse, resp.getWriter());
    }

    public static void badRequest(HttpServletResponse resp, String message) throws IOException {
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        CordraErrorResponse errorResponse = new CordraErrorResponse(message);
        gson.toJson(errorResponse, resp.getWriter());
    }

    public static void okWithMessage(HttpServletResponse resp, String message) throws IOException {
        resp.setStatus(HttpServletResponse.SC_OK);
        CordraErrorResponse errorResponse = new CordraErrorResponse(message);
        gson.toJson(errorResponse, resp.getWriter());
    }
    
    public static void notFound(HttpServletResponse resp, String message) throws IOException {
        resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
        CordraErrorResponse errorResponse = new CordraErrorResponse(message);
        gson.toJson(errorResponse, resp.getWriter());
    }
}
