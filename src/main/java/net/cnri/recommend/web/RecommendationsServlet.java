package net.cnri.recommend.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.recommend.RecommenderService;
import net.cnri.recommend.ServiceFactory;

@WebServlet({"/recommendations/*"})
public class RecommendationsServlet extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(RecommendationsServlet.class);
    
    private static final int NUM_RECS = 10;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String authHeaderString = req.getHeader("Authorization");
            if (authHeaderString == null) {
                throw new BadRequestCordraException("Auth header is missing");
            }
            BasicAuthHeader authHeader = new BasicAuthHeader(authHeaderString);
            RecommenderService service = ServiceFactory.getInstance();
            String userId = service.getUserIdForUsername(authHeader.getUsername());
            String boostString = req.getParameter("boost");
            boolean boost = false;
            if ("true".equals(boostString)) {
                boost = true;
            }
            String modeString = req.getParameter("mode");
            RecommenderService.Mode mode = RecommenderService.Mode.SIMPLE_CONTENT_BASED;
            if ("similarProfiles".equals(modeString)) {
                mode = RecommenderService.Mode.SIMILAR_PROFILE_BASED;
            }
            List<String> recommendations = service.getRecommendations(
                userId, NUM_RECS, boost, mode, authHeader.getUsername(), authHeader.getPassword());
            Gson gson = new Gson();
            String result = gson.toJson(recommendations);
            PrintWriter w = resp.getWriter();
            w.write(result);
            w.close();
        } catch (BadRequestCordraException e) {
            ServletErrorUtil.badRequest(resp, e.getMessage());
        } catch (Exception e) {
            logger.error("Unexpected error", e);
            ServletErrorUtil.internalServerError(resp);
        } 
    }
}
