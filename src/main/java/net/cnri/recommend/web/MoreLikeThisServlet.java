package net.cnri.recommend.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.recommend.RecommenderService;
import net.cnri.recommend.ServiceFactory;

@WebServlet({"/moreLikeThis/*"})
public class MoreLikeThisServlet extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(MoreLikeThisServlet.class);
    
    private static final int NUM_RECS = 10;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String authHeaderString = req.getHeader("Authorization");
            if (authHeaderString == null) {
                throw new Exception("Auth header is missing");
            }
            BasicAuthHeader authHeader = new BasicAuthHeader(authHeaderString);
            RecommenderService service = ServiceFactory.getInstance();
            String userId = service.getUserIdForUsername(authHeader.getUsername());
            String numString = req.getParameter("num");
            int num = NUM_RECS;
            if (numString != null) {
                num = Integer.parseInt(numString);
            }
            String[] idsArray = req.getParameterValues("id");
            if (idsArray == null || idsArray.length == 0) {
                throw new BadRequestCordraException("Request must include one or more id params.");
            }
            List<String> ids = Arrays.asList(idsArray);
            List<String> results = service.getMoreLikeThis(
                userId, ids, authHeader.getUsername(), authHeader.getPassword(), num);
            Gson gson = new Gson();
            String result = gson.toJson(results);
            PrintWriter w = resp.getWriter();
            w.write(result);
            w.close();
        } catch (BadRequestCordraException e) {
            ServletErrorUtil.badRequest(resp, e.getMessage());
        } catch (Exception e) {
            logger.error("Unexpected error", e);
            ServletErrorUtil.internalServerError(resp);
        } 
    }
}
