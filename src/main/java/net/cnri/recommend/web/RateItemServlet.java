package net.cnri.recommend.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.recommend.RecommenderService;
import net.cnri.recommend.ServiceFactory;

@WebServlet({"/rateItem/*"})
public class RateItemServlet extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(RateItemServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String authHeaderString = req.getHeader("Authorization");
            if (authHeaderString == null) {
                throw new BadRequestCordraException("Auth header is missing");
            }
            BasicAuthHeader authHeader = new BasicAuthHeader(authHeaderString);
            RecommenderService service = ServiceFactory.getInstance();
            String userId = service.getUserIdForUsername(authHeader.getUsername());
            String itemId = req.getParameter("itemId");
            if (itemId == null) {
                throw new BadRequestCordraException("Request must include itemId param");
            }
            String ratingString = req.getParameter("rating");
            Integer rating = null;
            if (ratingString != null) {
                try {
                    rating = Integer.parseInt(ratingString);
                } catch (NumberFormatException e) {
                    throw new BadRequestCordraException(ratingString + ", is not a valid rating");
                }
            }
            service.rateItem(userId, itemId, rating, authHeader.getUsername(), authHeader.getPassword());
            ServletErrorUtil.okWithMessage(resp, "success");
        } catch (BadRequestCordraException e) {
            ServletErrorUtil.badRequest(resp, e.getMessage());
        } catch (Exception e) {
            logger.error("Unexpected error", e);
            ServletErrorUtil.internalServerError(resp);
        } 
    }
    
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String authHeaderString = req.getHeader("Authorization");
            if (authHeaderString == null) {
                throw new Exception("Auth header is missing");
            }
            BasicAuthHeader authHeader = new BasicAuthHeader(authHeaderString);
            RecommenderService service = ServiceFactory.getInstance();
            String userId = service.getUserIdForUsername(authHeader.getUsername());
            String itemId = req.getParameter("itemId");
            
            service.deleteRating(userId, itemId, authHeader.getUsername(), authHeader.getPassword());
            ServletErrorUtil.okWithMessage(resp, "success");
        } catch (Exception e) {
            logger.error("Unexpected error", e);
            ServletErrorUtil.badRequest(resp, e.getMessage());
        } 
    }
}
