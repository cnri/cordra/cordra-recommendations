/*************************************************************************\
    Copyright (c) 2019 Corporation for National Research Initiatives;
                        All rights reserved.
\*************************************************************************/

package net.cnri.recommend.web;

import com.google.gson.Gson;
import net.cnri.cordra.api.CordraClient;
import net.cnri.cordra.api.TokenUsingHttpCordraClient;
import net.cnri.recommend.Config;
import net.cnri.recommend.RecommenderService;
import net.cnri.recommend.ServiceFactory;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.File;
import java.io.FileReader;

@WebListener
public class WebappStartupListener implements ServletContextListener {

    private static Logger logger = LoggerFactory.getLogger(RateItemServlet.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            ServletContext context = sce.getServletContext();
            File webappsStorageDir = getWebappsStorageDirectoryAsFile(context);
            File configFile = new File(webappsStorageDir, "recommendations-config.json");
            Gson gson = new Gson();
            Config config = gson.fromJson(new FileReader(configFile), Config.class);

            CordraClient cordra = new TokenUsingHttpCordraClient(config.cordraBaseUri, "admin", config.cordraAdminPassword);
            SolrClient solr = new HttpSolrClient.Builder().withBaseSolrUrl(config.solrBaseUri).build();
            RecommenderService service = ServiceFactory.initialize(config.cordraBaseUri, cordra, solr, config.enableSimilarProfileMode);
            System.out.println("Recommender Service started");
        } catch (Exception e) {
            System.out.println("Something went wrong during Recommender Service startup. Check the error.log for details");
            logger.error("Something went wrong during Recommender Service startup", e);
            throw new RuntimeException(e);
        }
    }

    private File getWebappsStorageDirectoryAsFile(ServletContext context) {
        File webappStorage = (File) context.getAttribute("net.cnri.servercontainer.webapp_storage_directory");
        webappStorage.mkdirs();
        return webappStorage;
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        //no-op
    }
}
