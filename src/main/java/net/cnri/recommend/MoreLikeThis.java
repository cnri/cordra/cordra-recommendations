package net.cnri.recommend;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Map.Entry;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.util.NamedList;

import com.google.gson.Gson;

public class MoreLikeThis {

    public static Map<String, Double> getTermVector(String objectId, SolrClient solr, List<String> includeList) throws Exception {
        return getTermVectors(Collections.singletonList(objectId), solr, includeList).get(objectId);
    }

    public static Map<String, Map<String, Double>> getTermVectors(List<String> ids, SolrClient solr, List<String> includeList) throws Exception {
        long totalDocs = getTotalDocs(solr);
        Map<String, Map<String, Double>> res = new HashMap<>();
        String queryString = queryStringForIds(ids);
        SolrQuery query = new SolrQuery(fixSlashes(queryString));
        query.setRows(Integer.MAX_VALUE);
        query.setRequestHandler("/tvrh");
        query.setParam("tv.tf", true);
        query.setParam("tv.df", true);
        QueryResponse queryResponse = solr.query(query);
        if (queryResponse.getStatus() != 0) {
            throw new Exception("Unexpected Solr response "  + queryResponse);
        }
        NamedList termVectorsList = (NamedList) queryResponse.getResponse().get("termVectors");
        for (String id : ids) {
            NamedList termVectorNamedList = (NamedList) termVectorsList.get(id);
            Map<String, Double> termVector = termVectorFromSolrNamedList(termVectorNamedList, totalDocs, includeList);
            res.put(id, termVector);
        }
        return res;
    }

    private static String queryStringForIds(List<String> ids) {
        return "id:" + String.join(" id:", ids);
    }

    private static Map<String, Double> termVectorFromSolrNamedList(NamedList termVector, long totalDocs,
        List<String> includeList) {
        Map<String, Double> result = new HashMap<>();
        for(Iterator<Entry<String, Object>> fi = termVector.iterator(); fi.hasNext(); ){
            Entry<String, Object> fieldEntry = fi.next();
            String key = fieldEntry.getKey();
            if(!"uniqueKey".equals(key)){
                if (includeList == null || includeList.contains(key) || containsPrefixFor(includeList, key)) {
                    for(Iterator<Entry<String, Object>> tvInfoIt = ((NamedList)fieldEntry.getValue()).iterator(); tvInfoIt.hasNext(); ){
                        Entry<String, Object> tvInfo = tvInfoIt.next();
                        NamedList tv = (NamedList) tvInfo.getValue();
                        String tvKey = tvInfo.getKey();
                        tvKey = quote(tvKey);
                        int tf = (int) tv.get("tf");
                        int df = (int) tv.get("df");
                        double idf = idf(df, totalDocs);
                        double tfidf = tf * idf;
                        result.put(key + ":" + tvKey, tfidf);
                    }
                }
            }       
        }
        return result;
    }

    private static boolean containsPrefixFor(List<String> includeList, String term) {
        for (String s : includeList) {
            if (s.endsWith("*")) {
                String prefix = s.substring(0, s.length()-1);
                if (term.startsWith(prefix)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static String quote(String tvKey) {
        return tvKey.replace(":", "\\:");
    }

    public static long getTotalDocs(SolrClient solr) throws SolrServerException, IOException {
        SolrQuery q = new SolrQuery("*:*");
        q.setRows(0); 
        return solr.query(q).getResults().getNumFound();
    }

    public static double idf(long docFreq, long totalDocs) {
        return Math.log(totalDocs/(double)(docFreq+1)) + 1.0;
    }

    public static String getMoreLikeThis(List<String> ids, SolrClient solr) throws Exception {
        Map<String, Map<String, Double>> termVectorsMap = MoreLikeThis.getTermVectors(ids, solr, null);
        String query = buildMoreLikeThisQueryFor(termVectorsMap.values(), true);
        return query;
    }

    public static String buildMoreLikeThisQueryFor(Map<String, Double> termVector, boolean includeBoost) {
        PriorityQueue<Map.Entry<String, Double>> priorityTerms = toSortedPriortiyQueue(termVector);
        List<Map.Entry<String, Double>> priorityTermsList = new ArrayList<>();

        int maxItems = 25;
        for (int i = 0; i < maxItems; i++) {
            Entry<String, Double> term = priorityTerms.poll();
            if (term == null) {
                break;
            }
            priorityTermsList.add(term);
        }

        StringBuilder sb = new StringBuilder();
        Entry<String, Double> lastTerm = priorityTermsList.get(priorityTermsList.size()-1);
        double lowestScore = lastTerm.getValue();
        for (Entry<String, Double> term : priorityTermsList) {
            double score = term.getValue();
            double nScore = score / lowestScore;
            sb.append(term.getKey());
            if (includeBoost) {
                sb.append("^"+nScore);
            }
            sb.append(" ");
        }
        return sb.toString();
    }

    public static String buildMoreLikeThisQueryForWithBoosts(Map<String, Map<String, Double>> termVectors, Map<String, Double> boosts) {
        Map<String, Map<String, Double>> normalizedVectors = VectorUtil.normalizeTermVectors(termVectors);
        boostTermVectors(boosts, normalizedVectors);
        List<Map<String, Double>> normalizedVectorsAsList = new ArrayList<>();
        normalizedVectorsAsList.addAll(normalizedVectors.values());
        Map<String, Double> tasteVector = VectorUtil.sumTermVectors(normalizedVectorsAsList);
        String query = buildMoreLikeThisQueryFor(tasteVector, true);
        return query;
    }

    public static void boostTermVectors(Map<String, Double> itemRatingMap, Map<String, Map<String, Double>> termVectorsMap) {
        for (String itemId : itemRatingMap.keySet()) {
            Double rating = itemRatingMap.get(itemId);
            Double boost = rating;
            Map<String, Double> termVector = termVectorsMap.get(itemId);
            for (String term : termVector.keySet()) {
                Double magnitude = termVector.get(term);
                Double boostedMagnitude = magnitude * boost;
                termVector.put(term, boostedMagnitude);
            }
        }
    }

    public static String buildMoreLikeThisQueryFor(Collection<Map<String, Double>> termVectors, boolean includeBoost) {
        Map<String, Double> tasteVector = VectorUtil.normalizeAndSumTermVectors(termVectors);
        String query = buildMoreLikeThisQueryFor(tasteVector, includeBoost);
        return query;
    }

    public static String generateExcludeIdsQueryFragment(List<String> ids) {
        return " -id:" + String.join(" -id:", ids);
    }

    private static PriorityQueue<Map.Entry<String, Double>> toSortedPriortiyQueue(Map<String, Double> termVector) {
        PriorityQueue<Map.Entry<String, Double>> priorityTerms = new PriorityQueue<>(11, new TermComparator());
        for (Entry<String, Double> term : termVector.entrySet()) {
            priorityTerms.add(term);
        }
        return priorityTerms;
    }

    public static String fixSlashes(String s) {
        if(s==null) return null;
        return s.replaceAll("(?<!\\\\)/","\\\\/");
    }

    public static class TermComparator implements Comparator<Map.Entry<String, Double>> {
        @Override
        public int compare(Entry<String, Double> a, Entry<String, Double> b) {
            return  Double.compare(b.getValue(), a.getValue());
        }
    }
}
