package net.cnri.recommend;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VectorUtil {
	public static Map<String, Double> sumTermVectors(Map<String, Double> a, Map<String, Double> b) {
		Map<String, Double> result = new HashMap<>();
		result.putAll(a);
		
		for (Map.Entry<String, Double> entry : b.entrySet()) {
			String key = entry.getKey();
			Double resultValue = result.get(key);
			double bValue = b.get(key);
			if (resultValue == null) {
				result.put(key, bValue);
			} else {
				double sum = resultValue + bValue;
				result.put(key, sum);
			}
		}
		return result;
	}
	
	public static Map<String, Double> sumTermVectors(List<Map<String, Double>> vectors) {
		Map<String, Double> result = new HashMap<>();
		for (int i = 0; i < vectors.size(); i++) {
			Map<String, Double> v = vectors.get(i);
			if (i == 0) {
				result.putAll(v);
			} else {
				result = sumTermVectors(result, v);
			}
		}
		return result;
	}	
	
	public static Map<String, Double> sumWeightedTermVectors(List<WeightedTermVector> wVectors) {
		List<Map<String, Double>> vectors = new ArrayList<>();
		for (WeightedTermVector wVector : wVectors) {
			Map<String, Double> vector = normalizeTermVector(wVector.termVector);
			vector = multiplyTermVector(vector, wVector.weight);
			vectors.add(vector);
		}
		Map<String, Double> result = sumTermVectors(vectors);
		return result;
	}
	
	public static Map<String, Double> multiplyTermVector(Map<String, Double> v, Double weight) {
		Map<String, Double> result = new HashMap<>();
		for (Map.Entry<String, Double> entry : v.entrySet()) {
			String key = entry.getKey();
			double value = entry.getValue();
			double weightedValue = value * weight;
			result.put(key, weightedValue);
		}
		return result;
	}
	
	/**
	 * convert to unit vector
	 */
	public static Map<String, Double> normalizeTermVector(Map<String, Double> v) {
		Double runningSumOfSquares = 0d;
		for (Map.Entry<String, Double> entry : v.entrySet()) {
			double value = entry.getValue();
			double square = value * value;
			runningSumOfSquares += square;
		}
		double weight = 1 / Math.sqrt(runningSumOfSquares);
		Map<String, Double> result = multiplyTermVector(v, weight);
		return result;
	}
	
	public static List<Map<String, Double>> normalizeTermVectors(Collection<Map<String, Double>> vectors) {
		List<Map<String, Double>> resultList = new ArrayList<Map<String, Double>>();
		for (Map<String, Double> v : vectors) {
			Map<String, Double> nv = normalizeTermVector(v);
			resultList.add(nv);
		}
		return resultList;
	}
	
	public static Map<String, Map<String, Double>> normalizeTermVectors(Map<String, Map<String, Double>> namedVectors) {
	    Map<String, Map<String, Double>> resultMap = new HashMap<>();
	    for (String name : namedVectors.keySet()) {
	        Map<String, Double> v = namedVectors.get(name);
	        Map<String, Double> nv = normalizeTermVector(v);
	        resultMap.put(name, nv);
	    }
	    return resultMap;
	}
	
	public static Map<String, Double> normalizeAndSumTermVectors(Collection<Map<String, Double>> vectors) {
		List<Map<String, Double>> normalizedVectors = normalizeTermVectors(vectors);
		Map<String, Double> result = sumTermVectors(normalizedVectors);
		return result;
	}	
	
}
