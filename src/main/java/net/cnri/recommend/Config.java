package net.cnri.recommend;

public class Config {

    public String cordraBaseUri;
    public String solrBaseUri;
    public String cordraAdminPassword;
    public boolean enableSimilarProfileMode = false;
}
