package net.cnri.recommend;

import org.apache.solr.client.solrj.SolrClient;

import net.cnri.cordra.api.CordraClient;


public class ServiceFactory {

    private static RecommenderService instance = null;

    public static RecommenderService getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Service is not initialized");
        }
        return instance;
    }

    public static synchronized RecommenderService initialize(String cordraBaseUri, CordraClient cordra, SolrClient solr, boolean enableSimilarProfileMode) {
        instance = new RecommenderService(cordraBaseUri, cordra, solr, enableSimilarProfileMode);
        return instance;
    }
}
