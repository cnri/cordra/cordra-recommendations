package net.cnri.recommend;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.JsonElement;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.cnri.cordra.api.CordraClient;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.HttpCordraClient;
import net.cnri.cordra.api.SearchResults;

public class Recommender {

    public static final int THREADS = 16; 
    
    public static SearchResults<CordraObject> getSimilarProfileBasedRecommendations(String profileId, CordraClient cordra, SolrClient solr, List<String> limitedFields, boolean useRatingsBoost) throws Exception {
        CordraObject co = cordra.get(profileId);
        JsonObject profile = co.content.getAsJsonObject();
        JsonArray itemRatings = profile.get("itemRatings").getAsJsonArray();
        if (itemRatings == null || itemRatings.size() == 0) {
            return null; 
        }
        
        //build a MoreLikeThis query for the current users profile
        List<String> ids = Collections.singletonList(profileId);
        Map<String, Map<String, Double>> termVectorsMap = MoreLikeThis.getTermVectors(ids, solr, null);
        //consider only using terms that start with "/itemRatings"
        
        String similarProfilesQueryString = MoreLikeThis.buildMoreLikeThisQueryFor(termVectorsMap.values(), true);
        String excludeIds = MoreLikeThis.generateExcludeIdsQueryFragment(ids);
        similarProfilesQueryString = similarProfilesQueryString + excludeIds;
        similarProfilesQueryString = MoreLikeThis.fixSlashes(similarProfilesQueryString);
        similarProfilesQueryString = escapeTildes(similarProfilesQueryString);

        SolrQuery query = new SolrQuery(similarProfilesQueryString);
        query.set("fl", "score,id");
        QueryResponse queryResponse = solr.query(query);
        if (queryResponse.getStatus() != 0) {
            throw new Exception("Unexpected Solr response "  + queryResponse);
        }
        SolrDocumentList profilesDocumentList = queryResponse.getResults();

        List<String> similarProfileIds = new ArrayList<>();
        //select only those with a good score
        //What is a good score?
        //For now just get the 5 most similar profiles.
        int similarProfileLimit = 5;
        for (int i = 0; i < profilesDocumentList.size(); i++) {
            SolrDocument solrProfile = profilesDocumentList.get(i);
            String id = (String) solrProfile.getFieldValue("id");
            //Float score = (Float) solrProfile.getFieldValue("score");
            //System.out.println(score + " " + id);
            similarProfileIds.add(id);
            if (i == similarProfileLimit) {
                break;
            }
        }
        Map<String, Map<String, Double>> similarUsersItemRatings = getUserItemRatingsFor(similarProfileIds, cordra);
        Map<String, Double> thisUserItemRatings = getItemRatingMap(profile);
        Set<String> allRatedItemIds = new HashSet<>();
        for (String similarUserId : similarUsersItemRatings.keySet()) {
            Map<String, Double> similarUserItemRatings = similarUsersItemRatings.get(similarUserId);
            allRatedItemIds.addAll(similarUserItemRatings.keySet());
        }
        allRatedItemIds.addAll(thisUserItemRatings.keySet());
        List<String> allRatedItemIdsAsList = new ArrayList<>();
        allRatedItemIdsAsList.addAll(allRatedItemIds);
        Map<String, Map<String, Double>> similarItemsTermVectorsMap = MoreLikeThis.getTermVectors(allRatedItemIdsAsList, solr, limitedFields);
        String queryBasedOnSimilarProfiles = null;
        if (useRatingsBoost) {
            Map<String, Double> boostItemRatingMap = getAvgBoostMap(similarUsersItemRatings, thisUserItemRatings);
            queryBasedOnSimilarProfiles = MoreLikeThis.buildMoreLikeThisQueryForWithBoosts(similarItemsTermVectorsMap, boostItemRatingMap);
        } else {
            Collection<Map<String, Double>> itemTerms = similarItemsTermVectorsMap.values();
            queryBasedOnSimilarProfiles = MoreLikeThis.buildMoreLikeThisQueryFor(itemTerms, true);
        }
        
        List<String> likedIds = new ArrayList<>();
        for (int i = 0; i < itemRatings.size(); i++) {
            JsonObject itemRating = itemRatings.get(i).getAsJsonObject();
            String id = itemRating.get("id").getAsString();
            likedIds.add(id);
        }
        
        List<String> idsThisUserHadAlreadyRatedExcludeIds = likedIds;
        String excludeItemIds = generateExcludeDoisQueryFragment(idsThisUserHadAlreadyRatedExcludeIds);
        queryBasedOnSimilarProfiles = queryBasedOnSimilarProfiles + excludeItemIds;
        queryBasedOnSimilarProfiles = "(" + queryBasedOnSimilarProfiles + ") -type:User -type:Schema";
        return cordra.search(queryBasedOnSimilarProfiles);
    }
    
    private static Map<String, Double> getWeightedAvgBoostMap(Map<String, Map<String, Double>> similarUsersItemRatings, Map<String, Double> thisUserItemRatings,
        double similarUsersWeight, double userWeight) {
        Map<String, Integer> countMap = new HashMap<>();
        Map<String, Double> sumMap = new HashMap<>();
        for (String profileId : similarUsersItemRatings.keySet()) {
            Map<String, Double> itemRatings = similarUsersItemRatings.get(profileId);
            for (String itemId : itemRatings.keySet()) {
                Double rating = itemRatings.get(itemId);
                count(countMap, itemId);
                sum(sumMap, itemId, rating * similarUsersWeight);
            }
        }
        for (String itemId : thisUserItemRatings.keySet()) {
            Double rating = thisUserItemRatings.get(itemId);
            count(countMap, itemId);
            sum(sumMap, itemId, rating * userWeight);
        }
        Map<String, Double> avgRatingMap = new HashMap<>();
        for (String itemId : countMap.keySet()) {
            Integer count = countMap.get(itemId);
            Double sum = sumMap.get(itemId);
            Double avg = sum / count;
            avgRatingMap.put(itemId, avg);
        }
        return avgRatingMap;
    }

    private static Map<String, Double> getAvgBoostMap(Map<String, Map<String, Double>> similarUsersItemRatings, Map<String, Double> thisUserItemRatings) {
        Map<String, Integer> countMap = new HashMap<>();
        Map<String, Double> sumMap = new HashMap<>();
        for (String profileId : similarUsersItemRatings.keySet()) {
            Map<String, Double> itemRatings = similarUsersItemRatings.get(profileId);
            for (String itemId : itemRatings.keySet()) {
                Double rating = itemRatings.get(itemId);
                count(countMap, itemId);
                sum(sumMap, itemId, rating);
            }
        }
        for (String itemId : thisUserItemRatings.keySet()) {
            Double rating = thisUserItemRatings.get(itemId);
            count(countMap, itemId);
            sum(sumMap, itemId, rating);
        }
        Map<String, Double> avgRatingMap = new HashMap<>();
        for (String itemId : countMap.keySet()) {
            Integer count = countMap.get(itemId);
            Double sum = sumMap.get(itemId);
            Double avg = sum / count;
            avgRatingMap.put(itemId, avg);
        }
        return avgRatingMap;
    }
    
    private static void count(Map<String, Integer> countMap, String itemId) {
        Integer count = countMap.get(itemId);
        if (count == null) {
            count = 0;
        }
        count = count + 1;
        countMap.put(itemId, count);
    }
    
    private static void sum(Map<String, Double> sumMap, String itemId, Double value) {
        Double sum = sumMap.get(itemId);
        if (sum == null) {
            sum = 0d;
        }
        sum = sum + value;
        sumMap.put(itemId, sum);
    }

    private static Map<String, Map<String, Double>> getUserItemRatingsFor(List<String> profileIds, CordraClient cordra) throws CordraException {
        Map<String, Map<String, Double>> userItemRatings = new HashMap<>();
        for (String profileId : profileIds) {
            CordraObject profileCo = cordra.get(profileId);
            JsonObject profile = profileCo.content.getAsJsonObject();
            Map<String, Double> itemRatingMap = getItemRatingMap(profile);
            userItemRatings.put(profileId, itemRatingMap);
        }
        
        return userItemRatings;
    }

    private static String escapeTildes(String similarProfilesQueryString) {
        return similarProfilesQueryString.replace("~", "\\~");
    }

    public static String generateExcludeDoisQueryFragment(List<String> ids) {
        return " -id:" + String.join(" -id:", ids);
    }

    public static void main(String[] args) throws Exception {
        String solrBaseUri = "http://big.local:8983/solr/cordra";
        SolrClient solr = new HttpSolrClient.Builder().withBaseSolrUrl(solrBaseUri).build();

        String cordraBaseUri = "http://big.local:8082/cordra/";
        CordraClient cordra = new HttpCordraClient(cordraBaseUri, "admin", "password");

        int numResults = 10;
        //generateRecommendationsForAllSimilarProfileBased(solr, cordra, numResults, staticLimitedFieldsForItems, Pipeline.ProfileMethod.LIMITED_FIELDS);
        
        boolean useRatingsBoost = true;
        
        try (SearchResults<CordraObject> results = getSimilarProfileBasedRecommendations("test/UserProfile-281987729.1505441025", cordra, solr, null, useRatingsBoost);) {
            int count = 1;
            for (CordraObject co : results) {
                System.out.println(co.id);
                count++;
                if (count > numResults) break; 
            }   
        }
        System.out.println();
    }
    
    public static Map<String, List<String>> generateRecommendationsForAllSimilarProfileBased(SolrClient solr, CordraClient cordra, int numResults, List<String> limitedFields, boolean useRatingsBoost) throws CordraException, Exception {
        Map<String, List<String>> recommendationsMap = new HashMap<>();
        List<String> profileIds = getProfileIdsList(cordra);

        ExecutorService exec = Executors.newFixedThreadPool(THREADS);

        int total = profileIds.size();
        for (String profileId: profileIds) {
//            System.out.println(profileId);
            List<String> userRecommendations = new ArrayList<>();
            recommendationsMap.put(profileId, userRecommendations);
            
            exec.submit(()-> {
                try (SearchResults<CordraObject> results = getSimilarProfileBasedRecommendations(profileId, cordra, solr, limitedFields, useRatingsBoost);) {
                    int count = 1;
                    for (CordraObject co : results) {
                        //System.out.println(co.id);
                        userRecommendations.add(co.id);
                        count++;
                        if (count > numResults) break; 
                    }   
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                printProgress(total, profileId);
                //System.out.println(".");
            });

        }
        exec.shutdown();
        try {
            exec.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return recommendationsMap;
    }

    private static AtomicInteger progress = new AtomicInteger(0);
    
    public synchronized static void printProgress(int total, String id) {
        int count = progress.incrementAndGet();
//        System.out.println(count + "/" + total + "\t" + id);
    }
    
    public static List<String> getProfileIdsList(CordraClient cordra) throws CordraException {
        List<String> profileIds = new ArrayList<>();
        try (SearchResults<String> profileIdsResults = cordra.searchHandles("type:User");) {
            for (String profileId: profileIdsResults) {
                profileIds.add(profileId);
            }
        }
        return profileIds;
    }
    
    public static Map<String, List<String>> generateRecommendationsForAllSimpleContentBased(SolrClient solr, CordraClient cordra, int numResults, List<String> limitedFields, boolean useRatingsBoost) throws Exception {
        ExecutorService exec = Executors.newFixedThreadPool(THREADS);
        
        Map<String, List<String>> recommendationsMap = new HashMap<>();
        List<String> profileIds = getProfileIdsList(cordra);
        int total  = profileIds.size();
        for (String profileId: profileIds) {
            //System.out.println(profileId);
            List<String> userRecommendations = new ArrayList<>();
            recommendationsMap.put(profileId, userRecommendations);
            
            exec.submit(() -> {
            try (SearchResults<CordraObject> results = getSimpleContentBasedPersonalRecommendations(profileId, cordra, solr, limitedFields, useRatingsBoost);) {
                int count = 1;
                for (CordraObject co : results) {
                    //System.out.println(co.id);
                    userRecommendations.add(co.id);
                    count++;
                    if (count > numResults) break; 
                }   
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            printProgress(total, profileId);
            });
        }
        
        exec.shutdown();
        try {
            exec.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return recommendationsMap;
    }
    
    public static SearchResults<CordraObject> getSimpleContentBasedPersonalRecommendations(String profileId, CordraClient cordra, SolrClient solr, List<String> limitedFields, boolean useRatingsBoost) throws Exception {
        List<String> likedIds = new ArrayList<>();
        CordraObject profileCo = cordra.get(profileId);
        JsonObject profile = profileCo.content.getAsJsonObject();
        JsonArray itemRatings = profile.get("itemRatings").getAsJsonArray();

        for (int i = 0; i < itemRatings.size(); i++) {
            JsonObject itemRating = itemRatings.get(i).getAsJsonObject();
            String id = itemRating.get("id").getAsString();
            likedIds.add(id);
        }
        
//        JsonArray itemIds = profile.get("ids").getAsJsonArray();
//        for (int i = 0; i < itemIds.size(); i++) {
//            String id = itemIds.get(i).getAsString();
//            likedIds.add(id);
//        }
        
        //Map<ItemId> Map<term, magnitude>>
        Map<String, Map<String, Double>> termVectorsMap = MoreLikeThis.getTermVectors(likedIds, solr, limitedFields);
        String queryString = null;
        if (useRatingsBoost) {
            Map<String, Double> itemRatingMap = getItemRatingMap(profile);
            queryString = MoreLikeThis.buildMoreLikeThisQueryForWithBoosts(termVectorsMap, itemRatingMap);
        } else {
            queryString = MoreLikeThis.buildMoreLikeThisQueryFor(termVectorsMap.values(), true);
        }
        String excludeIdsQueryFragment = MoreLikeThis.generateExcludeIdsQueryFragment(likedIds);
        
        queryString = queryString + excludeIdsQueryFragment;
        queryString = MoreLikeThis.fixSlashes(queryString);
        queryString = "(" + queryString + ") -type:User -type:Schema";
        return cordra.search(queryString);
    }

    private static Map<String, Double> getItemRatingMap(JsonObject profile) {
        Map<String, Double> itemRatingMap = new HashMap<>();
        JsonElement ratingsElem = profile.get("itemRatings");
        if (ratingsElem != null) {
            JsonArray itemRatings = ratingsElem.getAsJsonArray();
            for (int i = 0; i < itemRatings.size(); i++) {
                JsonObject itemRating = itemRatings.get(i).getAsJsonObject();
                String id = itemRating.get("id").getAsString();
                Double rating;
                if (itemRating.has("rating")) {
                    rating = itemRating.get("rating").getAsDouble();
                } else {
                    rating = 1D;
                }
                itemRatingMap.put(id, rating);
            }
        }
        return itemRatingMap;
    }  
    
    public static Double scaleRatingToBoostFactor(Double rating) {
        return rating;
    }
    
    public static void boostTermVectorsByRatings(Map<String, Double> itemRatingMap, Map<String, Map<String, Double>> termVectorsMap) {
        for (String itemId : itemRatingMap.keySet()) {
            Double rating = itemRatingMap.get(itemId);
            Double boost = scaleRatingToBoostFactor(rating);
            Map<String, Double> termVector = termVectorsMap.get(itemId);
            //if (termVector != null) {
                for (String term : termVector.keySet()) {
                    Double magnitude = termVector.get(term);
                    Double boostedMagnitude = magnitude * boost;
                    termVector.put(term, boostedMagnitude);
                }
            //}
        }
    }

    public static SearchResults<CordraObject> moreLikeThis(List<String> ids, CordraClient cordra, SolrClient solr) throws Exception {
        String queryString = MoreLikeThis.getMoreLikeThis(ids, solr);
        String excludeIdsQueryFragment = MoreLikeThis.generateExcludeIdsQueryFragment(ids);
        queryString = queryString + excludeIdsQueryFragment;
        queryString = MoreLikeThis.fixSlashes(queryString);
        return cordra.search(queryString);
    }
}
